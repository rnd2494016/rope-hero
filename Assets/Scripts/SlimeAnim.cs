using System;
using System.Collections;
using DG.Tweening;
using UnityEngine;

public class SlimeAnim : MonoBehaviour
{
    public Transform associateObject;
    public float movementDistance = 0.2f;
    public float movementDuration = 1.5f;
    private void Start()
    {
        StartCoroutine(Anim());
    }
    
    IEnumerator Anim()
    {
        while (true)
        {
            Vector3 destination = GameManager.Instance.player.transform.position;
            float distance = Vector3.Distance(destination,transform.parent.position);
            destination += new Vector3(GameManager.Instance.player.safeRadius - 0.2f, 0, 0);
            destination.y = transform.parent.position.y;
            if (distance > GameManager.Instance.player.safeRadius)
            {
                float distanceToPlayer = Vector3.Distance(transform.position, destination);
                float duration = Mathf.Clamp(distanceToPlayer / movementDistance, 0.1f, movementDuration);
                transform.parent.DOMove(destination, duration).SetEase(Ease.Linear);
            }
            else
            {
                if(!GameManager.Instance.player.canShoot)
                {
                    GameManager.Instance.player.anim.SetTrigger("Fire");
                    GameManager.Instance.player.canShoot = true;
                }
            }
            
            if (associateObject)
            {
                associateObject.DOMoveY(associateObject.position.y + 0.2f, 0.5f).OnComplete(() =>
                {
                    associateObject.DOMoveY(associateObject.position.y - 0.2f, 0.4f);
                });
            }
            transform.DOMoveY(transform.position.y + 0.2f, 0.5f).OnComplete(() =>
            {
                transform.DOMoveY(transform.position.y - 0.2f, 0.4f);
            });
            
            transform.DOScale(new Vector3(0.3f, 0.5f, 0.3f), .5f).OnComplete(() =>
            {
                transform.DOScale(new Vector3(0.4f, 0.3f, 0.4f), .4f).OnComplete(() =>
                {
                    transform.DOScale(new Vector3(0.3f, 0.45f, 0.3f), .3f).OnComplete(() =>
                    {
                        transform.DOScale(new Vector3(0.4f, 0.4f, 0.4f), .3f);
                    });
                });
            });
            yield return new WaitForSeconds(2f);
        }
    }

    private void OnDestroy()
    {
        transform.DOKill();
        associateObject.DOKill();
    }
}
