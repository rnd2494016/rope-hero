using System;
using System.Collections;
using System.Collections.Generic;
using CodeMoney_HowToHealthSystem_1_Final;
using DG.Tweening;
using TMPro;
using UnityEngine;

public class PlayerShooting : MonoBehaviour
{
    public GameObject bulletPrefab;
    public Transform firePoint;
    public float bulletForce = 20f;
    public float shootingDelay = 1f;
    public List<Transform> enemy;

    public bool canShoot = false;
    public Animator anim;
    public ParticleSystem particleSystem;
    public Coroutine coroutine;
    public float safeRadius = 10f;
    public Transform healthBar;
    public TMP_Text healthText;
    [HideInInspector] public float maxHealth = 100f;
    private static readonly int Fire = Animator.StringToHash("Fire");
    private static readonly int Idle = Animator.StringToHash("Idle");

    public void Start()
    {
        healthText.text = maxHealth.ToString();
        anim.SetBool("isWalking", true);
        coroutine = StartCoroutine(ShootWithDelay());
    }

    IEnumerator ShootWithDelay()
    {
        while (true)
        {
            while (canShoot && enemy.Count != 0)
            {
                anim.SetBool("isWalking", false);
                yield return new WaitForSeconds(shootingDelay/4);
                anim.SetTrigger(Fire);
                particleSystem.Play();
                Bounds bound = enemy[0].GetComponentInChildren<Collider>().bounds;
                Vector3 position = bound.center;
                position.y += bound.size.y/4;
                Shoot(position - firePoint.position, position);

                yield return new WaitForSeconds(shootingDelay);
            }
            yield return null;
        }
    }

    void Shoot(Vector3 direction, Vector3 targetEnemy)
    {
        GameObject bullet = Instantiate(bulletPrefab, firePoint.position, Quaternion.identity);
        bullet.transform.LookAt(targetEnemy);
        Rigidbody rb = bullet.GetComponent<Rigidbody>();
        
        if (rb != null)
        {
            bullet.transform.DOMove(targetEnemy, 0.4f).SetEase(Ease.Linear);
        }
        else
        {
            Debug.LogError("Bullet prefab does not have a Rigidbody component!");
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Vector3 center = transform.position;
        int segments = 30;

        float angleIncrement = 360f / segments;
        float angle = 0f;

        Vector3 prevPoint = center + new Vector3(safeRadius, 0f, 0f);

        for (int i = 0; i <= segments; i++)
        {
            float x = Mathf.Cos(Mathf.Deg2Rad * angle) * safeRadius;
            float z = Mathf.Sin(Mathf.Deg2Rad * angle) * safeRadius;

            Vector3 currentPoint = center + new Vector3(x, 0f, z);

            if (i > 0)
            {
                Gizmos.DrawLine(prevPoint, currentPoint);
            }

            prevPoint = currentPoint;
            angle += angleIncrement;
        }
    }
}