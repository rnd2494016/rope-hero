using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Obi;
using UnityEngine;
using Random = UnityEngine.Random;

public class Ball : MonoBehaviour
{
    public float bounceForce = 0.03f;
    public void MoveToPipe()
    {
        Vector3 pipe = GameManager.Instance.pipe.position;
        Vector3 position = transform.position;
        Vector3 middle = new Vector3((pipe.x + position.x) / 2, (pipe.y + position.y) / 2 + 3f, (pipe.z + position.z) / 2 - 2f);
        pipe.y += 0.45f;
        Vector3[] wayPoints = { position, middle, pipe };
        transform.DOPath(wayPoints, 1f, PathType.CatmullRom).SetDelay(Random.Range(0.6f, 0.8f)).OnComplete(() =>
        {
            int count = int.Parse(GameManager.Instance.pipeText.text) + 1;
            GameManager.Instance.pipeText.transform.DOPunchScale(new Vector3(1.05f, 1.05f, 1.05f), 0.1f, 1, 0).OnComplete(
                () =>
                {
                    GameManager.Instance.pipeText.transform.localScale = Vector3.one;
                });
            GameManager.Instance.pipeText.text = count.ToString();
            transform.DOKill();
            GetComponent<Rigidbody>().velocity = Vector3.zero;
        }).SetEase(Ease.Linear);
    }
}