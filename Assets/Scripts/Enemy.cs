using System;
using System.Globalization;
using DG.Tweening;
using TMPro;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float maxHealth = 30f;
    public Transform healthBar;
    public TMP_Text healthText;
    public Material material;
    public int slimeCount = 2;
    public ParticleSystem hitParticle, deadParticle;
    [SerializeField] private Enemy2 enemyPrefab;
    protected float health;
    private Vector3[] slimePositions = { new(-0.9f, 0, 1.5f), new(0.2f, 0, 0) };

    public void Start()
    {
        GetComponentInChildren<MeshRenderer>().material = material;
        health = maxHealth;
        healthText.text = health.ToString(CultureInfo.InvariantCulture);
    }
    
    protected virtual void OnCollisionEnter(Collision other)
    {
        if (other.collider.CompareTag("Bullet"))
        {
            other.transform.DOKill();
            Destroy(other.gameObject);
            hitParticle.Play();
            health -= GameManager.Instance.attackStrength;
            healthText.text = health <= 0 ? 0.ToString() : Mathf.Ceil(health).ToString(CultureInfo.InvariantCulture);
            healthBar.localScale = new Vector3(GetHealthPercent(), 1);
            if (health <= 0)
            {
                GameManager.Instance.player.canShoot = false;
                if (slimeCount == 0)
                {
                    GameManager.Instance.player.enemy.Remove(transform);
                    Destroy(gameObject, 0.1f);
                }
                else InstantiateSlime();
            }
        }
    }
    
    private void InstantiateSlime()
    {
        Vector3 scale = transform.localScale * 3 / 4;
        transform.DOScale(0, 0.2f).OnComplete(() =>
        {
            GameManager.Instance.player.enemy.Remove(transform);
            Destroy(gameObject);
        });
        for (int i = 0; i < slimeCount; i++)
        {
            Enemy enemy = Instantiate(enemyPrefab);
            enemy.material = material;
            var transform1 = enemy.transform;
            transform1.position = transform.position;
            transform1.localScale = scale;
            enemy.maxHealth = maxHealth / 2;
            Vector3 newPosition = transform1.position + slimePositions[i];
            transform1.DOMove(newPosition, 0.4f);
            GameManager.Instance.player.enemy.Add(transform1);
        }
    }
    
    protected float GetHealthPercent()
    {
        float ratio = (float)health / maxHealth;
        return ratio <= 0 ? 0 : ratio;
    }

    private void OnDestroy()
    {
        ParticleSystem particle = Instantiate(deadParticle);
        particle.transform.position = transform.position;
        particle.Play();
        if (slimeCount == 0)
            GameManager.Instance.BallSpawn(2, transform.position);
    }
}
