using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class LevelComplete : MonoBehaviour
{
    private float timeToComplete = 0.8f;
    private Coroutine _coroutine;
    public ParticleSystem particleSystem;
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.name.Contains("Ball") && !GameManager.Instance.isLevelComplete)
        {
            GameManager.Instance.busketText.text = (int.Parse(GameManager.Instance.busketText.text) + 1).ToString();
            GameManager.Instance.busketText.transform.DOPunchScale(new Vector3(1.05f, 1.05f, 1.05f), 0.1f, 1, 0).OnComplete(
                () =>
                {
                    GameManager.Instance.busketText.transform.localScale = Vector3.one;
                });
            GameManager.Instance.totalScore += 1;
            GameManager.Instance.scoreText.text = GameManager.Instance.totalScore.ToString();
            particleSystem.Play();
            Destroy(other.gameObject);
            timeToComplete += 0.05f;
            if(_coroutine is not null) StopCoroutine(_coroutine);
            _coroutine = StartCoroutine(EnableNewSkill());
        }
    }

    private IEnumerator EnableNewSkill()
    {
        yield return new WaitForSeconds(timeToComplete);
        timeToComplete = 0.8f;
        GameManager.Instance.isLevelComplete = true;
        GameManager.Instance.newSkillPanel.SetActive(true);
    }
}
