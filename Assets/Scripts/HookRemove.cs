using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using Obi;
using UnityEngine;

namespace RND
{
    public class HookRemove : MonoBehaviour
    {
        public enum PinType
        {
            EndPoint,
            MiddlePoint
        }
        public List<ObiParticleAttachment> attachments;
        public PinType pinType;

        private void OnMouseDown()
        {
            if(!GameManager.Instance.isInputEnable) return;
            GameManager.Instance.SetRopeGravity(-9.81f);
            if (pinType == PinType.EndPoint)
            {
                foreach (ObiParticleAttachment a in from attachment in attachments select attachment.GetComponents<ObiParticleAttachment>() into att from a in att where a.target == transform select a)
                {
                    a.enabled = false;
                    Destroy(a);
                }
                GameManager.Instance.isHookRemoved = true;
                Vector3 position = transform.position;
                transform.DOMove(new Vector3(position.x, position.y, position.z - 1), 0.3f).SetEase(Ease.Linear);
                transform.DORotate(new Vector3(0, 0, 360), 0.3f, RotateMode.LocalAxisAdd).SetEase(Ease.Linear).OnComplete(() =>
                {
                    Destroy(gameObject);
                });
            }
            else
            {
                foreach (ObiParticleAttachment a in from attachment in attachments select attachment.GetComponents<ObiParticleAttachment>() into att from a in att where a.target == transform select a)
                {
                    a.enabled = false;
                    Destroy(a);
                }
                GameManager.Instance.isHookRemoved = true;
                Vector3 position = transform.position;
                transform.DOMove(new Vector3(position.x + 0.5f, position.y + 0.5f, position.z - 1), 0.3f).SetEase(Ease.Linear);
                transform.DORotate(new Vector3(10, -10, 25), 0.3f, RotateMode.LocalAxisAdd).SetEase(Ease.Linear).OnComplete(() =>
                {
                    Destroy(gameObject);
                });
            }
            
        }
        
    }
}

