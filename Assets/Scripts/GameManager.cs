using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Obi;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
    public List<GameObject> leveList = new List<GameObject>();
    public int currentLevel = 1;
    public PlayerShooting player;
    public Enemy enemyPrefab;
    public GameObject pipeBase, ballCountdownCollider, ballPrefab, journeyInProgress, gameHud, autoPlay, newSkillPanel;
    public Transform pipe, ballParent;
    public TMP_Text pipeText, busketText;
    public Text scoreText;
    public float delay = 2f;
    public bool isHookRemoved, isInputEnable = true, isStartMiniGame;
    public List<Material> enemyMaterials = new List<Material>();
    [HideInInspector] public int totalScore = 0;
    [HideInInspector] public float attackStrength = 10f;
    // [HideInInspector] public float gravity = -9.81f;
    [HideInInspector] public bool isLevelComplete;

    private GameObject newLevel;
    private List<GameObject> ballList = new List<GameObject>();

    
    #region Singleton
    private static GameManager _instance;
    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<GameManager>();
                if (_instance == null)
                {
                    GameObject gameManager = new GameObject("GameManager");
                    _instance = gameManager.AddComponent<GameManager>();
                }
            }
            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
        }

        Application.targetFrameRate = 60;
    }
    #endregion Singleton
    
    private IEnumerator BallFall()
    {
        yield return new WaitForSeconds(delay);
        pipeBase.SetActive(false);
        ballCountdownCollider.SetActive(true);
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && isHookRemoved)
        {
            StartCoroutine(BallFall());
            isHookRemoved = isInputEnable = false;
        }
        if(player.enemy.Count == 0 && !isStartMiniGame)
        {
            isStartMiniGame = true;
            player.canShoot = false;
            player.anim.SetTrigger("Idle");
            player.anim.enabled = false;
            Invoke(nameof(StartMiniGame), 2f);
        }
    }
    
    public void BallSpawn(int ballCount, Vector3 position)
    {
        Vector3[] slimePositions = { new Vector3(-0.04f, 0.3f, 0), new Vector3(0.04f, 0.3f, 0) };

        List<Ball> balls = new List<Ball>();
        for (int i = 0; i < ballCount; i++)
        {
            GameObject ballInstance = Instantiate(ballPrefab, ballParent);
            ballInstance.transform.position = position + slimePositions[i];
            balls.Add(ballInstance.GetComponent<Ball>());
            ballList.Add(ballInstance);
        }
        pipe.DOMove(new Vector3(1.15f, pipe.position.y, pipe.position.z), 0.3f).SetEase(Ease.Linear).OnComplete(() =>
        {
            foreach (var ball in balls)
            {
                ball.MoveToPipe();
            }
        });
    }

    public void SetRopeGravity(float g)
    {
        newLevel.GetComponentInChildren<ObiSolver>().gravity = new Vector3(0, g, 0);
    }

    private void StartMiniGame()
    {
        newLevel = Instantiate(leveList[currentLevel - 1], gameHud.transform);
        SetRopeGravity(-2f);
        Vector3 camPosition = Camera.main.transform.position;
        journeyInProgress.SetActive(false);
        gameHud.SetActive(true);
        Camera.main.transform.DOMove(new Vector3(camPosition.x, 3.4f, camPosition.z), 2f).OnComplete(() =>
        {
            isInputEnable = true;
        });
    }

    public void NextLevel()
    {
        Reset();
        isLevelComplete = false;
        currentLevel++;
        if (currentLevel > 2) currentLevel = 2;
        Enemy newEnemy = Instantiate(enemyPrefab, autoPlay.transform);
        newEnemy.material = enemyMaterials[1];
        player.enemy.Add(newEnemy.transform);
        isStartMiniGame = false;
        newEnemy.transform.position = new Vector3(4f, 9.56f, 2.32f);
        newEnemy.transform.rotation = Quaternion.Euler(0,45,0);
        newEnemy.Start();
        player.transform.rotation = Quaternion.Euler(0, 102.2f, 0);
        player.anim.enabled = true;
        player.anim.StopPlayback();
        player.canShoot = false;
        player.StopAllCoroutines();
        player.Start();
    }

    private void Reset()
    {
        DOTween.KillAll();
        Destroy(newLevel);
        isInputEnable = isHookRemoved = false;
        pipeBase.SetActive(true);
        ballCountdownCollider.SetActive(false);
        pipe.position = new Vector3(-4.74f, 7f, -3.15f);
        gameHud.SetActive(false);
        player.enemy = new List<Transform>();
        journeyInProgress.SetActive(true);
        pipeText.text = busketText.text = 0.ToString();
        foreach (var ball in ballList)
        {
            Destroy(ball);
        }
        ballList.Clear();
    }

    public void OnSkillPanelEnable()
    {
        Vector3 camPosition = Camera.main.transform.position;
        Camera.main.transform.DOMove(new Vector3(camPosition.x, 9f, camPosition.z), 1f);
    }
}
