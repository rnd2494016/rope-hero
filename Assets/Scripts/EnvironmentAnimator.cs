﻿using UnityEngine;

public class EnvironmentAnimator : MonoBehaviour
{
    public float speed;
    private float value;
    
    void Start()
    {
        value = 0f;
    }

    void Update()
    {
        if(!GameManager.Instance.player.anim.GetBool("isWalking") || GameManager.Instance.player.enemy.Count == 0) return;
        value += Time.deltaTime * speed;
        GetComponent<Renderer>().material.mainTextureOffset = new Vector2(value, 0f);
    }
}
