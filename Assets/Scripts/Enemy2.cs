using System.Globalization;
using DG.Tweening;
using TMPro;
using UnityEngine;

public class Enemy2 : Enemy
{
    protected override void OnCollisionEnter(Collision other)
    {
        if (other.collider.CompareTag("Bullet"))
        {
            other.transform.DOKill();
            Destroy(other.gameObject);
            health -= GameManager.Instance.attackStrength;
            healthText.text = health <= 0 ? 0.ToString() : Mathf.Ceil(health).ToString(CultureInfo.InvariantCulture);
            healthBar.localScale = new Vector3(GetHealthPercent(), 1);
            if (health <= 0)
            {
                GameManager.Instance.player.enemy.Remove(transform);
                Destroy(gameObject, 0.1f);
            }
        }
    }
}
