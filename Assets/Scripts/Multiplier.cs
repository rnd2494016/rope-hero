using System;
using System.Collections;
using System.Collections.Generic;
using Obi;
using UnityEngine;
using Random = UnityEngine.Random;

public class Multiplier : MonoBehaviour
{
    public int multiplierAmount = 1;
    public GameObject ballPrefab;
    public Transform parentObject;

    private void OnTriggerExit(Collider other)
    {
        if(!other.gameObject.CompareTag("Ball")) return;
        other.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
        StartCoroutine(BallMultiply());
    }

    private IEnumerator BallMultiply()
    {
        for (int i = 0; i < multiplierAmount-1; i++)
        {
            GameObject ball = Instantiate(ballPrefab, parentObject);
            ball.transform.position = transform.position + new Vector3(Random.Range(-.5f, .5f),Random.Range(0f, 0.3f), 0f);
            yield return new WaitForSeconds(0.3f);
        }
    }
}
