using System.Collections;
using System.Collections.Generic;
using Obi;
using UnityEngine;

public class RopeCollider : MonoBehaviour
{
    public void OnContactEnterProcess(ObiSolver solver, Oni.Contact contact)
    {
        ObiColliderBase other = ObiColliderWorld.GetInstance().colliderHandles[contact.bodyB].owner;
        if (other.gameObject.CompareTag("Ball"))
        {
            Ball ball = other.GetComponent<Ball>();
            float bounceForce = ball.bounceForce;
            if(bounceForce <= 0) return;
            other.GetComponent<Rigidbody>().AddForce(transform.up * bounceForce, ForceMode.Impulse);
            ball.bounceForce -= 0.005f;
        }
    }
}
