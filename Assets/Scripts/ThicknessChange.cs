using System;
using System.Collections;
using System.Collections.Generic;
using Obi;
using UnityEngine;

public class ThicknessChange : MonoBehaviour
{
    public ObiRopeExtrudedRenderer obiRopeExtrudedRenderer;

    private void OnMouseDown()
    {
        if(!GameManager.Instance.isInputEnable) return;
        obiRopeExtrudedRenderer.thicknessScale = 5f;
    }
}
