using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class BallCountdown : MonoBehaviour
{
    private void OnTriggerExit(Collider other)
    {
        if (!other.gameObject.CompareTag("Ball")) return;
        GameManager.Instance.pipeText.text = (int.Parse(GameManager.Instance.pipeText.text) - 1).ToString();
        GameManager.Instance.pipeText.transform.DOPunchScale(new Vector3(0.95f, 0.95f, 0.95f), 0.1f, 1, 0).OnComplete(
            () =>
            {
                GameManager.Instance.pipeText.transform.localScale = Vector3.one;
            });
    }
}
