using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewSkill : MonoBehaviour
{
    private void OnEnable()
    {
        GameManager.Instance.OnSkillPanelEnable();
    }

    public void AttackStrengthIncrease(float strength)
    {
        GameManager.Instance.attackStrength += GameManager.Instance.attackStrength * strength;
        GameManager.Instance.NextLevel();
    }
    
    public void HealthIncrease(float health)
    {
        GameManager.Instance.player.maxHealth += GameManager.Instance.player.maxHealth * health;
        GameManager.Instance.NextLevel();
    }
}
